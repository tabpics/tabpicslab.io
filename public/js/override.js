// Get Options

// chrome.topSites.get(top_sites);

var random_sub = [];

function restore_options() {

  var options = localStorage.getItem('options');
  if(options) {
    options = JSON.parse(options);
  } else {
    options = {
      show_title: true,
      show_sites: false,
      subreddits: true
    }
  }
  if (options.show_title == false) {
    $('.info').css('display', 'none');
  }
  if (options.show_sites == true) {
    $('.top_sites').css('display', 'inherit');
  }

  subreddit = options.subreddits;
  x = 0;
  for (var index in subreddit) {

    if (subreddit[index] == 1) {
      //    console.log(index);
      random_sub[x] = index;
      x++;
    }
  }

  subreddit = random_sub[Math.floor(Math.random() * random_sub.length)];
  if (subreddit == null) {
    subreddit = 'earthporn';
  }

  new_photo(subreddit);
  localStorage.setItem('options', JSON.stringify(options));
}

// Attempt to get a photo from local storage


var photo = localStorage.getItem('photo');

if (photo == null) {

  // If a photo doesn't exist, retrieve one from server and set it as localStorage item
  new_photo();

  var photo = localStorage.getItem('photo');

}

// Parse the image data
the_image = JSON.parse(photo);

$('.background').css("background-image", "url('" + the_image.data_uri + "')");

$('.title').attr("href", "http://reddit.com" + the_image.permalink);
$('.title').html(the_image.title);
$('.author').html("By " + the_image.author);
$('.ups').html(the_image.ups);
$('.info').css('opacity', '0.6');

function new_photo(subreddit) {
  console.log(subreddit);

  $.getJSON("http://tab.pics/api/" + subreddit, function (data) {

    if(data.data_uri.length < 2048) {
      console.info('found missing image on ' + subreddit, data);
      new_photo(subreddit);
    } else {
      var dataToStore = JSON.stringify(data);
      localStorage.removeItem('photo');
      localStorage.setItem('photo', dataToStore);
    }

  }).fail(function (jqxhr, textStatus, error) {
    console.error('Cannot get image from server', jqxhr, textStatus, error)
  });

}

function top_sites(data) {

  $.each(data, function (index, value) {

    $('.top_sites').append("<div class='site_container'><div class='site'><a href='" + value.url + "'>" + value.title.substring(0, 25) + "..<div class='url'>" + value.url + "</div></div></a></div>");
    index++;
    if (index == 5)
      return false;
  });

}


document.addEventListener('DOMContentLoaded', restore_options);
